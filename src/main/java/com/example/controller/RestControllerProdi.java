package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.ProdiModel;
import com.example.service.ProdiService;

@RestController
public class RestControllerProdi {
	@Autowired
	ProdiService prodiDAO;
	
	@RequestMapping(value ="rest/prodi", method = RequestMethod.GET)
    public ProdiModel viewProdi (Model model, @RequestParam(value = "kode", required = true) String kode_prodi)
    {
        ProdiModel prodi = prodiDAO.selectProdi (kode_prodi);
        return prodi;
        
        
    }

}
